import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable()
export class MainService {

private configUrl = environment.backendUrl;


  constructor(private http: HttpClient,private router: Router) {

   }

   getHttpOptions(){
       
    let httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          Authorization: localStorage.getItem('token') || ''
        })};

        return httpOptions;

   }

   handleError(err:HttpErrorResponse){

    if(err.status==403){
      alert('Sesion expirada, debe volver a iniciar sesion.');
      this.router.navigate(['/login']);
    }else{
      let textError = '';

        for(let e of err.error){
          textError+=`- ${e.msg}
`
        }

      alert(textError);

    }

   }
   //Users
   public login(rut:string,password:string){
    rut = this.sanitizeRut(rut)
     return  this.http.post(this.configUrl+'users/login',{rut,password},this.getHttpOptions());
   }
   public singup(email:string,password:string,confirmPassword:string,rut:string,name:string){
    rut = this.sanitizeRut(rut)
    return  this.http.post(this.configUrl+'users/singup',{email,name,rut,password,confirmPassword},this.getHttpOptions());
  }


  //Get data
   public getTx(){
    return  this.http.get(this.configUrl+'activity',this.getHttpOptions());
  }

  public getSummary(){        
    return  this.http.get(this.configUrl+'summary',this.getHttpOptions());
  }

  //transactions

  public deposit(amount:number,description:string){
    return  this.http.post(this.configUrl+'deposit',{amount,description},this.getHttpOptions());
  }

  public withdraw(amount:number,description:string){
    return  this.http.post(this.configUrl+'withdraw',{amount,description},this.getHttpOptions());
  }

  public transfer(amount:number,description:string,toUser:string,name:string){
    toUser = this.sanitizeRut(toUser);
    return  this.http.post(this.configUrl+'transfer',{amount,name,toUser,description},this.getHttpOptions());
  }


  sanitizeRut(_rut:string){

    _rut = _rut.replace(".","")
    _rut = _rut.replace("-","")
    return _rut;
  }



}
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ActivityComponent } from './activity/activity.component';
import { LoginComponent } from './login/login.component';
import {AuthGuard} from './auth/auth-guard.service';
import { AuthService } from './auth/auth.service';
import { DepositComponent } from './deposit/deposit.component';
import { WithdrawComponent } from './withdraw/withdraw.component';
import { TransferComponent } from './transfer/transfer.component';
import { SingupComponent } from './singup/singup.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'singup', component: SingupComponent },
  { path: '', component: ActivityComponent,canActivate: [AuthGuard]  },
  { path: 'deposit', component: DepositComponent,canActivate: [AuthGuard]  },
  { path: 'withdraw', component: WithdrawComponent,canActivate: [AuthGuard]  },
  { path: 'transfer', component: TransferComponent,canActivate: [AuthGuard]  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes,{ enableTracing: true })],
  providers: [AuthGuard,AuthService],
  exports: [RouterModule]
})
export class AppRoutingModule { }

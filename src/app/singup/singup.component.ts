import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MainService } from '../main.service';

@Component({
  selector: 'app-singup',
  templateUrl: './singup.component.html',
  styleUrls: ['./singup.component.scss']
})
export class SingupComponent implements OnInit {

  constructor(private http:MainService,private router:Router) { }

  public name = '';
  public email = '';
  public rut = '';
  public password = '';
  public confirmPassword = '';

  ngOnInit(): void {
  }

  singup(){
    
    this.http.singup(this.email,this.password,this.confirmPassword,this.rut,this.name).subscribe(response=>{
      alert('Usuario Registrado con exito!');
      this.router.navigate(['/login']);
    },err=>{
      this.http.handleError(err)

    })


  }
}

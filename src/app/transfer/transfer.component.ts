import { Component, OnInit } from '@angular/core';
import { MainService } from '../main.service';

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.scss']
})
export class TransferComponent implements OnInit {

  constructor(private http:MainService) { }
  public description = '';
  public rut = '';
  public amount = 0;
  public name = '';

  ngOnInit(): void {
  }
  
   
  transfer(){

    this.http.transfer(this.amount,this.description,this.rut,this.name).subscribe(data=>{

      alert('Transferencia realizada correctamente!')
      this.amount = 0;
      this.description = '';      

    },err=>{
      this.http.handleError(err)
    })
  }
}

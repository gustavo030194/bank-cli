import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { MainService } from '../main.service';

@Component({
  selector: 'app-deposit',
  templateUrl: './deposit.component.html',
  styleUrls: ['./deposit.component.scss']
})
export class DepositComponent implements OnInit {

  constructor(private http:MainService) { }
  amount:number = 0;
  description:string =''
  ngOnInit(): void {
  }

  deposit(){

    this.http.deposit(this.amount,this.description).subscribe(data=>{

      alert('Deposito realizado correctamente!')
      this.amount = 0;
      this.description = '';      

    },err=>{
      this.http.handleError(err)
    })
  }

}

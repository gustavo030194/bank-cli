export interface tx {

     id:string;
     createdAt:string;
     amount:number;
     type:string;
     description:string;
     currentAmount:number
  
  }
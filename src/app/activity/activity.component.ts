import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MainService } from '../main.service';
import { tx } from './tx.dto';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.scss']
})
export class ActivityComponent implements OnInit {

  public totalAmount = 0;
  public currentAmount = 0;

  public transactions:tx[] = [];
  private configUrl = 'http://localhost:3000/activity'
  constructor(private http: MainService ) { }

  ngOnInit(): void {
    this.getSummary();
  }


  async getTransactions(){
    this.http.getTx().subscribe((request:any)=>{
      this.transactions = request;
      this.currentAmount = this.totalAmount-0;
      for(let tx of request){
        tx.currentAmount = this.currentAmount;
        this.currentAmount = this.currentAmount-tx.amount;
      }
    },err=>{
      console.log(err)

    })
  }

  async getSummary(){
    this.http.getSummary().subscribe((request:any)=>{
      this.totalAmount = request;
      this.getTransactions()

    })
  }


}

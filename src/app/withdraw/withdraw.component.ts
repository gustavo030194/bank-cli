import { Component, OnInit } from '@angular/core';
import { MainService } from '../main.service';

@Component({
  selector: 'app-withdraw',
  templateUrl: './withdraw.component.html',
  styleUrls: ['./withdraw.component.scss']
})
export class WithdrawComponent implements OnInit {

  constructor(private http:MainService) { }
  public description = '';
  public amount = 0;
  ngOnInit(): void {
  }

  withdraw(){

    this.http.withdraw(this.amount,this.description).subscribe(data=>{

      alert('Deposito retirado correctamente!')
      this.amount = 0;
      this.description = '';      

    },err=>{
      this.http.handleError(err)
    })
  }
}

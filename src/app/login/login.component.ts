import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MainService } from '../main.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

public bankUser: string = '';
public bankPassword: string = '';
public texto = '';
  constructor( private router: Router,private http:MainService ) { 

  }

  ngOnInit(): void {

  }

  public  async login(){

 
this.http.login(this.bankUser,this.bankPassword).subscribe(
      (response:any) => {              
        localStorage.setItem('token',response.token); 
        this.router.navigate(['/']);

        return response;
      },
      (error) => {        
      this.http.handleError(error);  
      }
    )

 }


}

